import React from "react";
import clsx from "clsx";
import Link from "@docusaurus/Link";
import styles from "./HomepageFeatures.module.css";

const FeatureList = [
  {
    title: "Summary",
    Svg: require("../../static/img/undraw_receipt.svg").default,
    description: (
      <>
        As part of the "National contest for Swiss Youth in Science", a poster
        summarizing the paper was created. The poster is available on the{" "}
        <Link to="/downloads">Downloads Page</Link> or you can download it
        directly by{" "}
        <a
          target="\_blank"
          href={require("../../static/pdf/poster-summary-v-1_0_0.pdf").default}
        >
          clicking here
        </a>
        .
      </>
    ),
  },
  {
    title: "Voting Process",
    Svg: require("../../static/img/undraw_stepping_up.svg").default,
    description: (
      <>
        A summary of the voting process is available as a poster. The poster is
        available on the <Link to="/downloads">Downloads Page</Link> or you can
        download it directly by{" "}
        <a
          target="\_blank"
          href={
            require("../../static/pdf/poster-voting-process-v-1_0_0.pdf")
              .default
          }
        >
          clicking here
        </a>
        .
      </>
    ),
  },
  {
    title: "Contact",
    Svg: require("../../static/img/undraw_personal_email.svg").default,
    description: (
      <>
        If you have any questions or feedback, don't hesitate to contact me at{" "}
        <a href="mailto:levin.heimgartner@protonmail.com">
          levin.heimgartner@protonmail.com
        </a>
      </>
    ),
  },
];

function Feature({ Svg, title, description }) {
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
