---
title: Downloads
description: Download the pdf version of the documentation
hide_table_of_contents: true
---

# Downloads

<a
target="\_blank"
href={require('../../static/pdf/docs-v-1_0_0.pdf').default}>
PDF Version of the Documentation
</a>

<p></p>
<a
target="\_blank"
href={require('../../static/pdf/paper-v-2_0_0.pdf').default}>
Matura Paper (Version 2)
</a>

<p></p>

<a
target="\_blank"
href={require('../../static/pdf/poster-summary-v-1_0_0.pdf').default}>
Summary Poster
</a>

<p></p>

<a
target="\_blank"
href={require('../../static/pdf/poster-voting-process-v-1_0_0.pdf').default}>
Voting Process Poster
</a>

<p></p>

**To download any of the software, visit the project's source code repository.**
