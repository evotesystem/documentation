---
sidebar_position: 5
---

# Running Automated Contract Tests

Install the Ganache CLI tool

```bash
sudo npm install -g ganache-cli 
```

CD intro the `contract` folder inside the project

```bash
cd contract
```

Run the tests

```bash
npm run test
```

:::info

Depending on the system you are using, the tests can take a lot of time to complete.

:::