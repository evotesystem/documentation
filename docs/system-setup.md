---
sidebar_position: 1
---

# System Setup

:::info

🐧 This tutorial is for **Ubuntu Desktop 20.04.3**

:::

If you have a virtual machine running with the system already set up and the code downloaded, you can skip to the [Setup the Project](/docs/system-setup#setup-the-project) section.

If you already have a virtual machine setup and the code running, you can go to the [Testing the Voting Process](/docs/testing-voting-process/intro) tutorial.

## Setting up the System from Scratch

Open a terminal and install the node package manager

```bash
sudo apt install npm
```

Install NodeJS version 16.13 and open a new terminal afterwards

```bash
sudo npm cache clean -f
sudo npm install -g n
sudo n 16.13
sudo npm install -g npm@8.1.4
```

Install git

```bash
sudo apt install git
```

Install truffle

```bash
sudo npm install -g truffle
```

## Download the Code

Copy the project's source code from the archive in the USB drive to the development machine.

## Setup the Project

:::caution Info
All the commands of the evotesystem need to be run inside the root folder of the project!
:::

To install all the required npm packages and the cli tool, enter:

```bash
npm run setup
```

:::info
The installation of all the packages in all the submodules can take a while!
:::

## Install the Registration Application

To install the pre-built and packaged application, run:

```bash
npm run iregdeb
```

If you want to build and install the app yourself, run:

```bash
sudo npm i -g electron
npm run makeregdeb
```

## Configure the Project

Open the configuration file of the server and the contract at `config/config.json` . You can leave everything as it is, except the values for `MNEMONIC` and `PRIVATE_KEY`. 

The value for `MNEMONIC` is the mnemonic for the Ethereum account that will be used to deploy the contract to the Ropsten test network. 

The value for `PRIVATE_KEY` is the private key of the Ethereum account that will be used to relay the votes to the smart contract.

I already pre-funded an account with test Ether. You can use this account's mnemonic and private key in the configuration. The file should then look like this:

```json
{
    "contract": {
        "LOAD_ADMINS": "TRUE",
        "PUBLICNET": "FALSE",
        "MNEMONIC": "anxiety siren cancel absurd panic memory alpha badge sleep pear spin limit",
        "UPDATE_ABI": "FALSE"
    },
    "server": {
        "PORT": 8080,
        "PRIVATE_KEY": "0xcb6a95d047e23d391cd8abcb14c6f1520c3f30342258bb4c5d72e02d33149af3",
        "NODE_ENV": "development",
		"PROVIDER_PROXY": {
            "TARGET": "",
            "ENDPOINT": ""
        }
    }
}
```

Optionally you can update the `PORT` value, if you want to use a different port than the preconfigured one.

Now distribute the contract and server config:

```bash
npm run updateconfig
```

## Deploy the Evote Contract


The accounts that will be set as the admins on contract deployment are located in the `config/Admins.config.json` file. If you want to replace the existing admins with admins you generated yourself with the cli tool, this will work, however, if you do not know how to fund those accounts with test Ether, it is highly recommended that you don't change the admins and stick to the ones already listed in the file. The file also contains the private keys of the accounts. In production, the file would only contain the addresses for security reasons.


In order to deploy a contract instance to the Ropsten testnet, enter:

```bash
npm run deployropsten
```

:::caution important

Truffle will first compile the Evote contract and then deploy it. The contract is very large, that's why the deployment might take several minutes. It is **very important** that you'll save the **contract address** of the contract instance (Truffle will first deploy a contract called "Migrations", whose address you don't need, and after that a contract called "Evote", whose address you need to save). Truffle will tell you the address during deployment. You will need to save the address somewhere for the next step.

:::

## Configure the Evote Contract

Open the Evote contract configuration file located at `config/Evote.config.json`. You can leave everything as it is, except the value of `address`. This is the address of the deployed contract instance on the Ropsten test network.

Now enter the address that truffle returned after deploying the contract.

To distribute the Evote contract config and build the frontends, run:

```bash
npm run build
```

## Start the Server

To start the server with the relay and the frontends, enter:

```bash
npm run startserver
```

- The admin app should now be available at: `http://localhost:{PORT}/admin/`
- The voting app should now be available at: `http://localhost:{PORT}/client/`
- The documentation should now be available at: `http://localhost:{PORT}/d/`

:::tip info

The next time you want to test the frontends, you only need to run `npm run startserver`

You now finished the setup and can start testing 🥳

:::