---
sidebar_position: 1
---

# Adding Admins

## Requirements

In order to add an account as an admin, the admin group (registrars, chairpeople, electoral board) to which you want to add the admin to, needs to have **one member missing**.

## Adding a Registrar or a Chairperson

### Starting an Add Admin Request

1. Log in to the admin web app with an **electoral board member** account.
2. Click on `Admins` in the navbar. A list with all admins should appear.
3. If a registrar or chairperson is missing, an `Add Registrar` or `Add Chairperson` button will appear below the admin list.
4. Click on the `Add Registrar` or `Add Chairperson` button.
5. A pop-up should appear asking you to enter the address of the admin that should be added. 
6. Click on `Add Admin`.
7. A Metamask pop-up should appear asking you to confirm the transaction.

An alert should now have appeared on top of the admins page saying, that there is an open add admin request. The electoral board member that was used to create the request already voted on the request when it was created.

### Finishing an Add Admin Request

Add admin requests for registrars or chairpeople require a two-thirds majority of the electoral board. Since there are 6 electoral board members, 4 votes are required to add the admin. Because the electoral board member that started the request already voted on it, 3 more votes are required to add the admin. 

Since 3 more votes are required, **repeat the steps below 3 times**:

1. Disconnect the currently connected electoral board member account.
2. Log in to the admin web app with an electoral board member that hasn't voted on the request yet.
3. On top of the admins page, an alert should be displayed, asking you to vote on the add admin request.
4. Click on the `Add Admin` button in the alert.
5. A Metamask pop-up should appear asking you to confirm the transaction.

After you repeated the steps above 3 times and you refreshed the page, the alert should have disappeared and the admin should be displayed in the list with the other admins.


## Adding an Electoral Board Member

### Starting an Add Admin Request

1. Log in to the admin web app with a **registrar or chairperson** account.
2. Click on `Admins` in the navbar. A list with all admins should appear.
3. If an electoral board member is missing, an `Add Electoral Board Member` button will appear below the admin list.
4. Click on the `Add Registrar` or `Add Chairperson` button.
5. A pop-up should appear asking you to enter the address of the admin that should be added. 
6. Click on `Add Admin`.
7. A Metamask pop-up should appear asking you to confirm the transaction.

An alert should now have appeared on top of the admins page saying, that there is an open add admin request. The registrar or chairperson that was used to create the request already voted on the request when it was created.

### Finishing an Add Admin Request

Add admin requests for electoral board members require a two-thirds majority of the **registrars and the chairpeople combined**. Since there are 6 registrars and chairpeople, 4 votes are required to add the admin. Because the registrar or chairperson that started the request already voted on it, 3 more votes are required to add the electoral board member. 

Since 3 more votes are required, **repeat the steps below 3 times**:

1. Disconnect the currently connected registrar or chairperson account.
2. Log in to the admin web app with a registrar or chairperson that hasn't voted on the request yet.
3. On top of the admins page, an alert should be displayed, asking you to vote on the add admin request.
4. Click on the `Add Admin` button in the alert.
6. A Metamask pop-up should appear asking you to confirm the transaction.

After you repeated the steps above 3 times and you refreshed the page, the alert should have disappeared and the electoral board member should be displayed in the list with the other electoral board members.