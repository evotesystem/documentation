---
sidebar_position: 2
---

# Removing Admins

## Requirements

In order to remove an admin, all admin groups need to be **complete**:
* 3 Registrars
* 3 Chairpeople
* 6 Electoral Board Members

## Removing a Registrar or a Chairperson

### Starting an Admin Removal Request

1. Log in to the admin web app with an **electoral board member** account.
2. Click on `Admins` in the navbar. A list with all admins should appear.
3. If all admins are complete, a remove button should be displayed to the right of the registrars and chairpeople.
4. Click on the `Remove` button next to the admin you want to remove.
5. A pop-up should appear asking you to create a new request. Click on `Create Request`.
6. A Metamask pop-up should appear asking you to confirm the transaction.

An alert should now have appeared on top of the admins page saying that there is an open admin removal request. The electoral board member that was used to create the request already voted on the request, when it was created.

### Finishing an Admin Removal Request

Admin removal requests for registrars or chairpeople require a two-thirds majority of the electoral board. Since there are 6 electoral board members, 4 votes are required to remove the admin. Because the electoral board member that started the request already voted on it, 3 more votes are required to remove the admin. 

Since 3 more votes are required, **repeat the steps below 3 times**:

1. Disconnect the currently connected electoral board member account.
2. Log in to the admin web app with an electoral board member that hasn't voted on the request yet.
3. On top of the admins page, an alert should be displayed, asking you to vote on the admin removal request.
4. Click on the `Remove Admin` button in the alert.
6. A Metamask pop-up should appear asking you to confirm the transaction.

After you repeated the steps above 3 times, the alert should disappear and the admin should no longer be displayed in the list with the other admins.


## Removing an Electoral Board Member

### Starting an Admin Removal Request

1. Log in to the admin web app with a **registrar or chairperson** account.
2. Click on `Admins` in the navbar. A list with all admins should appear.
3. If all admins are complete, a remove button should be displayed to the right of the electoral board members.
4. Click on the `Remove` button next to the electoral board member you want to remove.
5. A pop-up should appear asking you to create a new request. Click on `Create Request`.
6. A Metamask pop-up should appear asking you to confirm the transaction.

An alert should now have appeared on top of the admins page saying that there is an open admin removal request. The registrar or chairperson that was used to create the request already voted on the request, when it was created.

### Finishing an Admin Removal Request

Admin removal requests for electoral board members require a two-thirds majority of the **registrars and chairpeople combined**. Since there are 6 registrars and chairpeople, 4 votes are required to remove the electoral board member. Because the registrar or chairperson that started the request already voted on it, 3 more votes are required to remove the electoral board member. 

Since 3 more votes are required, **repeat the steps below 3 times**:

1. Disconnect the currently connected registrar or chairperson.
2. Log in to the admin web app with a registrar or a chairperson that hasn't voted on the request yet.
3. On top of the admins page, an alert should be displayed, asking you to vote on the admin removal request.
4. Click on the `Remove Admin` button in the alert.
6. A Metamask pop-up should appear asking you to confirm the transaction.

After you repeated the steps above 3 times, the alert should disappear and the admin should no longer be displayed in the list with the other admins.