---
sidebar_position: 5
---

# Add the Verification Hashes

## Before you start

Unzip the `addresses.zip` file that was generated in the previous section.

## Steps

1. Login to the admin web app using a registrar account.
2. Click on the poll you want to add the verification hashes to.
3. Scroll down to the `Add Verification Hashes` button and click it.
4. On the modal that appears, choose the `meta.json` file that is located inside the `addresses` folder that should have been created when unzipping `addresses.zip`.
5. Verify that the information the modal provides is correct.
6. Click `Submit`. A Metamask pop-up should now appear asking you to confirm the transaction.
7. Wait until the website refreshes the UI and displays the verification hashes.
