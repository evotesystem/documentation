---
sidebar_position: 7
---

# Confirm Voters

:::info

Voters can't be modified after they have been confirmed.

:::

## Steps

### Start a Voter Confirmation Request

1. Login to the application with a chairperson account.
2. Select the poll whose voters you want to confirm.
3. An alert asking you to start a voter confirmation request should be displayed on top of the page.
4. Click on the `Start Request` button.
5. A Metamask pop-up should appear asking you to confirm the transaction.

The alert should now tell you that there is an open voter confirmation request. The account you used to open the request has automatically voted on the request, so you can't use it anymore to vote on it. Only chairpeople can vote on voter confirmation requests.

### Finish the Voter Confirmation Request

To finish a voter confirmation request, a two-third majority of the chairpeople is required. Since there are 3 chairpeople, 2 votes are required to pass the request. Because the chairperson that created the request already voted on it, only one more vote is required to pass the request and confirm the voters:

1. Disconnect the currently logged in chairperson from the admin web app.
2. Login to the admin web app using one of the two chairpeople accounts that didn't create the request.
3. Click on the poll for which you started the voter confirmation request before.
4. An alert should be displayed on top of the page asking you to vote on the voter confirmation request.
5. Click on the `Confirm Voters` button.
6. A Metamask pop-up should appear asking you to confirm the transaction.

When the transaction passes after a few seconds, the alert should disappear and the poll status should get updated to `Voters Confirmed`.
