---
sidebar_position: 3
---

# Create a Poll

## Requirements

You need to be logged in to the admin web app with the account of a chairperson. Read the [Connecting Metamask](/docs/testing-voting-process/metamask/connecting-metamask) section for help.

:::caution warning

The browser must display time inputs in `24 hours` format and not  `am/pm` for the application to work.

:::

## Steps

1. Click on the `Create Poll` button in the top right.
2. Enter the title of the poll you want to create.
3. Choose how many vote options there should be (between 2 and 5), and what those vote options should be.
4. Enter the opening, closing and tallying time.
5. Enter the content hash. (Feature not fully finished; just enter `0x42bb176eb31c9c20c3889601d6533eb9e7de06a6040dd2f9ea73381c5b21e0ce` as a value so the blockchain doesn't reject the transaction)
6. Enter the RSA public key you created before in the `Public Key` field. (You can simply rename the `public_key.pem` file to `public_key.txt` and then copy the key from there. You need to copy **everything** from inside the `pem` file.)
7. Finally, click on submit. A Metamask pop-up should appear, asking you to confirm the transaction. After you clicked confirm, you can go back to the poll overview.

After the transaction is processed, the polls overview page should refresh and display the new poll.