---
sidebar_position: 6
---

# Add Voters to the Poll

:::info

The smart contract won't allow voters to be added to or removed from a poll while the voters of the same poll are being confirmed.

:::

## Steps

1. Login to the admin web app with a chairperson account.
2. Click on the poll you want to add the voters to.
3. Scroll to the bottom of the page and click the `Add Voters` button.
4. Click on the `Choose file` button that appears and select the `addresses.json` file inside the `addresses` folder.
5. Click Submit. A Metamask pop-up should now appear asking you to confirm the transaction. Depending on the number of voters, the app might split the voters into multiple transactions, which you will all need to confirm separately.

Wait until the website refreshes and displays the voters on the bottom of the page to make sure the voters have been added successfully.
