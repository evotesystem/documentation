---
sidebar_position: 4
---

# Generate User Accounts

## Requirements

### Poll Created

You already need to have a poll created, as described in the previous section. You will need to enter the poll ID the poll title and the smart contract address when starting the account creation process. You can get the poll ID and the poll title by selecting the poll for which you want to register users. At the top the poll ID will be displayed with a # in  front of it. To the right of it, you will see the poll title. The contract address is below the title of the polls overview view.

### User CSV File Available

To create the user accounts, the application will ask you to import a `.csv` file containing all the users for which you want to create accounts for.

The header of the CSV file need to match the header below:

| firstName     | lastName      | street  | zip     | city     | canton     | country    |
|:-------------:|:-------------:|:-------:|:--------|:--------:|:----------:|:----------:|
| 1             | 2             | 3       | 4       |  5       | 6          | 7          |  

Sample CSV files with users are located in the evotesystem project at `assets/samples`. Note that if you use a CSV file with a lot of users, it will take longer to add the users.

### Registrar Private Keys

You'll need access to the registrar's private keys in order to sign the output of the Registration App.

# Steps

1. Open the Registration App and enter the poll ID, poll title and contract address when asked to. Optionally you can modify the voting link and the poll description.
2. Upload the CSV file containing the users and then click import.
3. Once the users are imported, you can verify if all the users have been correctly imported. Then click `Create Mnemonics and Access Letters`.
4. After the mnemonics and access letters have been generated, you'll need to enter the private keys of the registrars after another. (Order doesn't matter)
5. Click on continue. The files should now be signed. (This might take a while depending on the number of users)
6. Download all the files (`addresses.zip`, `users.zip`, `FirstActivationLetters.zip`, `SecondActivationLetters.zip`) and store them at a location where you won't loose them.
