---
sidebar_position: 8
---

# Confirm Poll

## Steps

### Starting the Poll Confirmation Request

1. Login to the admin web app with an electoral board account.
2. Select the poll you wan't to confirm.
3. An alert should be displayed on the top of the page asking you to open a poll confirmation request.
4. Click on the `Start Request` button.
5. A Metamask pop-up should appear asking you to confirm the transaction.

After the transaction has been processed, the alert should tell you that there is an open poll confirmation request. The account you used to open the request has automatically voted on the request, so you can't use it anymore to vote on it. Only electoral board member can vote on poll confirmation requests.

### Finishing the Poll Confirmation Request

To finish a poll confirmation request, a two-third majority of the electoral board is required. Since there are 6 chairpeople, 4 votes are required to pass the request. Because the electoral board member that created the request already voted on it, three more votes are required to pass the request and confirm the poll.

Since 3 more votes are required, **repeat the steps below 3 times**:

1. Disconnect the currently logged in electoral board member from the admin web app.
2. Login to the admin web app using an electoral board member that hasn't already voted on the request.
3. Click on the poll for which you started the poll confirmation request.
4. An alert should be displayed on top of the page asking you to vote on the poll confirmation request.
5. Click on the `Confirm Poll` button.
6. A Metamask pop-up should appear asking you to confirm the transaction.

After you repeated the steps above 3 times, and all the transactions have been confirmed, the alert should disappear and the poll status should get updated to `Poll Confirmed`.

**Congratulations! You successfully created an election :partying_face:**
