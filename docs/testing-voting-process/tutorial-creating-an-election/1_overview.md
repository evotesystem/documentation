---
sidebar_position: 1
---

# Overview

This tutorial is a step by step guide showing you how to start an election. It is assumed that you have full control over all admin accounts and that the election is only going to be used for **testing purposes**.


## Contents

Please follow the sections below **in order**, otherwise you might encounter problems.

1. [Generate RSA keys](/docs/testing-voting-process/tutorial-creating-an-election/generate-rsa-keys)
2. [Create Poll](/docs/testing-voting-process/tutorial-creating-an-election/create-poll)
3. [Generate User Accounts](/docs/testing-voting-process/tutorial-creating-an-election/generate-user-accounts)
4. [Add Verification Hashes](/docs/testing-voting-process/tutorial-creating-an-election/add-verification-hashes)
5. [Add Voters](/docs/testing-voting-process/tutorial-creating-an-election/add-voters)
6. [Confirm Voters](/docs/testing-voting-process/tutorial-creating-an-election/confirm-voters)
7. [Confirm Poll](/docs/testing-voting-process/tutorial-creating-an-election/confirm-poll)
