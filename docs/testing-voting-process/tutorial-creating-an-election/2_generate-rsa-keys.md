---
sidebar_position: 2
---

# Generate RSA Keys

## Steps

1. Open the CLI Tool in a terminal by entering `chainvote`
2. Choose the third option `[3] Create RSA Keys`
3. Enter the path where you want to save the keys to. Choose a location where you'll find them again later, like the desktop.
4. Enter the private keys of each Electoral Board member after another. (Order doesn't matter; Private Keys are located in `config/Admin.config.json`)
5. The RSA keys and a positions file should now have been generated and saved at the path entered in step 3. **Keep the keys and the positions file somewhere, where you won't loose them.**

:::danger

If you loose the RSA private key or the positions file, you won't be able to count the votes at the end of the election.

:::
