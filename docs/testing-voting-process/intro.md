---
sidebar_position: 1
---

# Intro

To test the election process, follow the steps below.

1. Setup Metamask and import the admin accounts with the help of the [Metamask Tutorial](/docs/testing-voting-process/metamask/setting-up-metamask)
2. Create an election using the [Creating an Election](/docs/testing-voting-process/tutorial-creating-an-election/overview) tutorial
3. Vote on the poll using the user accounts with the help of the [Voting](/docs/testing-voting-process/tutorial-voting) tutorial
4. Count the votes by following the [Counting the Votes](/docs/testing-voting-process/tutorial-counting-votes) tutorial
