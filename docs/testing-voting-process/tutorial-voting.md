---
sidebar_position: 5
---

# Tutorial 2 - Voting


## Logging In

### Requirements

* Voters generated: You are in possession of the access letters
* Voters have been added to the poll
* Poll confirmed

### Before you Start

Before you start the steps below, you need to unzip the `FirstActivationLetters.zip` archive and inside this archive, you need to unzip the `one.zip` archive, which contains all the first activation letters.
Then you need to unzip the `SecondActivationLetters.zip` archive and inside this archive, you need to unzip the `two.zip` archive, which contains all the second activation letters.

To cast a vote you need **both access letters of the same user**. The access letters are always named in the format `[one/two]_[zip]_[first name]_[last name]_[uuid].pdf`. After you have found both access letters of a user, you can start with the steps below.

### Steps

1. Visit the voting web app
2. Enter the mnemonics and the poll id
5. Click on `Login`

Now, depending on whether the account already voted on the poll or not, you will either be shown the details of the user's vote, or the vote options available and the `Submit Vote` button.

## Casting a Vote

### Steps

1. Choose one of the vote options
2. Click on `Submit Vote`
3. A pop-up will ask you to definitely submit the poll.

After you definitely submitted the vote a loading spinner will appear. After a few seconds, it should disappear and the app will show you the details of the vote's transaction and how the vote was generated.

Click the `Download Vote and View Details` button on the bottom of the page, which will take you to the verify vote page and will download a `.chainvote` file containing the details of the vote. You can verify the `.chainvote` file by following the steps in the "Verifying a Vote" section below.

## Verifying a Vote


### Verify Vote

After logging in to the voting web app, it will show you:
* Whether the vote has been counted or not
* The poll id of the poll the user voted on
* The address from which the vote has been cast (the address of the voter account)
* The hash of the vote
* The encrypted vote
* Details of the block in which the transaction was mined
* Details of the transaction
* How many votes every vote option received (*only if the election is over*)


### Verifying .chainvote File

If you want to know whether the vote has been generated and cast correctly, you can click the `Verify .chainvote File` button at the bottom of the page. A pop-up will appear asking you to choose the .chainvote file that you (probably) downloaded during the voting process. The app will start to verify the file immediately after selecting it. After verifying it, the app will tell you if the file could successfully be verified and will display you the poll id, the vote option id you voted for, and the UUID (salt) that was used to generate your vote. It will also tell you whether the data in the `.chainvote` file is correct or not.
