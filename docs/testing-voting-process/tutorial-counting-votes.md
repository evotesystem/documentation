---
sidebar_position: 6
---

# Tutorial 3 - Counting the Votes

## Requirements

* Poll is approved
* At least one user voted on the poll
* Encrypted RSA private key is available
* `positions.json` file is available (was generated with the RSA keys)
* Private keys of the electoral board are available


## Step 1 - Decrypting the RSA Private Key

1. Start the CLI tool from a command line by entering `chainvote`
2. Select the "Decrypt RSA Private Key" option from the menu
3. Enter the path of the `positions.json` file.
4. Enter the path of the encrypted RSA private key.
5. Enter the Ethereum private keys of the electoral board members

If everything is successful, the decrypted private key should have been **saved to the same folder**, where the encrypted private key was imported from.

## Step 2 - Counting the Votes with the CLI Tool

1. Start the CLI tool from a command line by entering `chainvote`
2. Select the "Count Votes" option from the menu
3. Enter the Ethereum private key of the electoral board member, whose account should be used to count the votes (use an account that has **enough ether** on it, to pay for the transaction fees. You should be able to take any of the Ethereum private keys located in the `config/Admins.config.json` file)
4. Enter the id of the poll, whose votes you want to count
5. Enter the path of the **decrypted** RSA private key

The tool will now display you how many votes it downloaded from the blockchain, how many votes haven't been counted yet, how many have already been counted and how many votes have an error.

6. Hit enter to decrypt the votes

The tool will now tell you how many of the votes that haven't been counted yet, have been decrypted.

7. Hit enter to count the votes

After submitting the transactions to count the votes, the CLI tool will tell you how many votes have been successfully counted and how many votes could not have been counted. 

You can see how many people voted for which option, by visiting the admin web app and selecting the poll whose votes you counted. The voters can also see how many votes every option received by logging in to the voting app after the votes have been counted. **It might take a few minutes for the transaction to process and for the admin and voting app to show the results!**
