---
sidebar_position: 1
---

# Setting Up Metamask

## Requirements

In order to install Metamask you need a browser that allows you to install the Metamask extension.

## Installing Metamask

1. Search for Metamask on the [Chrome](https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn) or [Firefox](https://addons.mozilla.org/en-US/firefox/addon/ether-metamask/) Web Store.

2. Click the `Add to Browser` button to install Metamask.

3. Follow the instructions in Metamask: Create a new account (you won't need this account) and choose a password, which you need to remember.
