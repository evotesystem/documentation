---
sidebar_position: 3
---

# Connecting Metamask

Before attempting to connect to the admin web app, make sure that Metamask is unlocked. You can do this by clicking on the Metamask extension. If it asks you to enter a password, it is locked. If it displays the user accounts, it is unlocked.

The eVote smart contract is deployed to the `Ropsten Test Network`, but by default Metamask uses the `Ethereum Mainnet`. To change this, you need to click on the Metamask extension. On the top of the extension a selector displays `Ethereum Mainnet`, click the selector and choose the `Ropsten Test Network`. **If you don't do this, the application will not work!**

To connect the admin web app to Metamask, you need to first visit the admin web app. Then you need to press the `Connect Metamask` button displayed in the middle of the admin app's login page.

After pushing the connect button a Metamask pop-up should appear asking you to choose an account to connect to the admin web app. Choose the account you want to use, but **make sure only one account is connected at a time**.

When you chose an account, you can press `Next` and afterwards `Connect`. The admin web app should get connected to Metamask and the pop-up should disappear.