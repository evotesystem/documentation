---
sidebar_position: 2
---

# Importing Admin Accounts


1. Open the `config/Admins.config.json` file located in the evotesystem project directory. The file contains the addresses _and the private keys (only in development)_ of the admin accounts of the deployed eVote contract.

2. Open Metamask and click the account icon (circle) in the top-right corner. Then select import account from the menu. Choose `Private Key` as type. Then copy the private key of the first registrar from `Admins.config.json` and paste it in Metamask. Then click `Import`.

3. After the account has been imported, select the account and click on the 3 dots below the account icon (circle) in the top-right corner. Then click on `Account Details` and click the edit icon next to the account name and rename it to `R 1`, short for Registrar 1.

4. Repeat steps 1 to 3 for all admins in the `Admins.config.json` file. Of course you should update the name in step 3 to match the admin you just imported (C for Chairpeople and EB for Electoral Board).

:::info

All the admin accounts should already have enough funds on them. However, if this is not the case, you can transfer funds between them using Metamask.

:::
