---
sidebar_position: 4
---

# Disconnecting Metamask

To disconnect an account from Metamask, you first need to click the Metamask extension. Then you need to select the account that is currently connected to the admin web app. When you select it, it will display a green dot and `Connected` to the left of the account name and address. Click on `Connected` and then on the 3 dots that are displayed to the right of the account name and click on `Disconnect this account`. The account should now get disconnected.
