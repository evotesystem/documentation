---
sidebar_position: 1
---

# Intro

Start with the [System Setup](/docs/system-setup) tutorial to setup a development system. After that you can go to the the [Testing the Voting Process](/docs/testing-voting-process/intro) tutorial, where you'll learn how to make an entire election or you can add and remove admins with the help of the [Testing Admin Modifications](/docs/admins/adding-admins) tutorial. After the system setup, you can also run the automated tests of the smart contract with the help of the [Running Automated Contract Tests](/docs/running-automated-tests) tutorial.
