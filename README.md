# Website

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

## Installation

```bash
npm install
```

## Local Development

```bash
npm start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

## Build

Create a `.env` file in the project. The file needs to contain the vercel url (the url where the website gets deployed to) and the base path of the application.

```
VERCEL_URL=
BASE_URL=
```

Then run:

```bash
npm run build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.
